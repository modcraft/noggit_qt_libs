<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- enginio_overview.qdoc -->
  <title>Enginio Manual 1.6</title>
  <link rel="stylesheet" type="text/css" href="style/offline-simple.css" />
  <script type="text/javascript">
    window.onload = function(){document.getElementsByTagName("link").item(0).setAttribute("href", "style/offline.css");};
  </script>
</head>
<body>
<div class="header" id="qtdocheader">
  <div class="main">
    <div class="main-rounded">
      <div class="navigationbar">
        <table><tr>
<td >Qt 1.6</td><td >Enginio Manual</td></tr></table><table class="buildversion"><tr>
<td id="buildversion" width="100%" align="right">Qt 1.6.0 Reference Documentation</td>
        </tr></table>
      </div>
    </div>
<div class="content">
<div class="line">
<div class="content mainContent">
  <link rel="next" href="enginio-getting-started.html" />
<p class="naviNextPrevious headerNavi">
<a class="nextPage" href="enginio-getting-started.html">Enginio Installation Notes and Prerequisites</a>
</p><p/>
<div class="sidebar">
<div class="toc">
<h3><a name="toc">Contents</a></h3>
<ul>
<li class="level1"><a href="#getting-started">Getting Started</a></li>
<li class="level1"><a href="#reference-documentation-and-examples">Reference documentation and examples:</a></li>
<li class="level1"><a href="#enginio-overview">Enginio Overview</a></li>
<li class="level2"><a href="#objects">Objects</a></li>
<li class="level2"><a href="#operations">Operations</a></li>
<li class="level2"><a href="#file-management">File Management</a></li>
</ul>
</div>
<div class="sidebar-content" id="sidebar-content"></div></div>
<h1 class="title">Enginio Manual</h1>
<span class="subtitle"></span>
<!-- $$$enginio-index.html-description -->
<div class="descr"> <a name="details"></a>
<p>This manual shows how to use the Enginio library in a Qt application. Both C++ and QML applications are covered. The manual is also used when integrating Enginio into an existing Qt project.</p>
<p>Enginio is a <a href="http://en.wikipedia.org/wiki/Backend_as_a_service">Backend-as-a-Service</a> solution for simplifying backend development of connected and data-driven applications.</p>
<a name="getting-started"></a>
<h2 id="getting-started">Getting Started</h2>
<p>First read the installation notes and prerequisites, then follow one of the short tutorials for using Enginio, either with QT Quick or with C++.</p>
<ul>
<li><a href="enginio-getting-started.html">Enginio Installation Notes and Prerequisites</a></li>
<li><a href="enginio-qml.html">Getting Started with Enginio using Qt Quick</a></li>
<li><a href="enginio-cpp.html">Getting Started with Enginio using C++</a></li>
</ul>
<a name="reference-documentation-and-examples"></a>
<h2 id="reference-documentation-and-examples">Reference documentation and examples:</h2>
<ul>
<li><a href="../qtenginio/enginio-cpp-classes-and-examples.html">Enginio C++ Classes and Examples</a></li>
<li><a href="../qtenginioqml/enginio-qml-types-and-examples.html">Enginio QML Types and Examples</a><p>Enginio documentation is also available at the <a href="https://developer.qtcloudservices.com">Qt Cloud Services</a> website <a href="http://engin.io/documentation/">here</a>.</p>
</li>
</ul>
<a name="enginio-overview"></a>
<h2 id="enginio-overview">Enginio Overview</h2>
<p>The Enginio APIs have a general pattern that is useful to understand. This section gives a short overview of the API concepts used throughout the library.</p>
<p>Enginio is a client-side library for communicating with the server at <a href="http://engin.io">http://engin.io</a>. Multiple <i>backends</i> can exist in each server account. When communicating with the server, the backend is specified by a <b>backend id</b>. When multiple users load an Enginio application, the all use the same <b>backend id</b>.</p>
<a name="objects"></a>
<h3 >Objects</h3>
<p>Enginio provides several types of objects. These include users, user groups, files, custom objects, and more. All communications with the backend use <a href="http://json.org">JSON</a>. When using the QML API, JSON objects are simply written inline. When using the C++ API, the <a href="../qtcore/qjsonobject.html">QJsonObject</a> and <a href="../qtcore/qjsonvalue.html">QJsonValue</a> classes are used.</p>
<p>Each JSON object in Enginio has the reserved properties <code>id</code>, <code>objectType</code>, <code>createdAt</code>, and <code>updatedAt</code>. For example, a custom object with no user-defined properties will look like this in JSON:</p>
<pre class="cpp">

  {
      <span class="string">&quot;id&quot;</span>: <span class="string">&quot;51cdbc08989e975ec300772a&quot;</span><span class="operator">,</span>
      <span class="string">&quot;objectType&quot;</span>: <span class="string">&quot;objects.todos&quot;</span><span class="operator">,</span>
      <span class="string">&quot;createdAt&quot;</span>: <span class="string">&quot;2013-06-28T16:38:32.369Z&quot;</span><span class="operator">,</span>
      <span class="string">&quot;updatedAt&quot;</span>: <span class="string">&quot;2013-06-28T16:38:32.725Z&quot;</span>
  }

</pre>
<p>But custom object types are normally augmented with user-defined properties that contain the application-specific data. A user-defined property can also contain a JSON object. For a more detailed description of Enginio objects, see the <a href="http://engin.io/documentation/overview/objects">Enginio Object documentation</a>.</p>
<a name="operations"></a>
<h3 >Operations</h3>
<p>The basic operations on objects are shown in the table below. For a complete list of operations see the <a href="../qtenginio/enginioclient.html">EnginioClient for C++</a> or the <a href="../qtenginioqml/qml-enginio-enginioclient.html">EnginioClient for QML</a>.</p>
<div class="table"><table class="generic">
 <thead><tr class="qt-style"><th >Operation</th><th >Description</th><th >C++</th><th >QML</th></tr></thead>
<tr valign="top" class="odd"><td >create</td><td >Insert a new object into the database</td><td ><a href="../qtenginio/enginioclient.html#create">EnginioClient::create</a>()</td><td ><a href="../qtenginioqml/qml-enginio-enginioclient.html#create-method">EnginioClient::create</a>()</td></tr>
<tr valign="top" class="even"><td >query</td><td >Query the database</td><td ><a href="../qtenginio/enginioclient.html#query">EnginioClient::query</a>()</td><td ><a href="../qtenginioqml/qml-enginio-enginioclient.html#query-method">EnginioClient::query</a>()</td></tr>
<tr valign="top" class="odd"><td >update</td><td >Update an object in the database</td><td ><a href="../qtenginio/enginioclient.html#update">EnginioClient::update</a>()</td><td ><a href="../qtenginioqml/qml-enginio-enginioclient.html#update-method">EnginioClient::update</a>()</td></tr>
<tr valign="top" class="even"><td >remove</td><td >Remove an object from the database</td><td ><a href="../qtenginio/enginioclient.html#remove">EnginioClient::remove</a>()</td><td ><a href="../qtenginioqml/qml-enginio-enginioclient.html#remove-method">EnginioClient::remove</a>()</td></tr>
</table></div>
<p><b>Note: </b>User and access control list management are also performed using these same functions.</p><a name="file-management"></a>
<h3 >File Management</h3>
<p>For file management, these operations are slightly different in that files are always attached to objects and can only be accessed through their objects. The Qt library provides convenient functions to upload and download files.</p>
</div>
<!-- @@@enginio-index.html -->
<p class="naviNextPrevious footerNavi">
<a class="nextPage" href="enginio-getting-started.html">Enginio Installation Notes and Prerequisites</a>
</p>
        </div>
       </div>
   </div>
   </div>
</div>
<div class="footer">
   <p>
   <acronym title="Copyright">&copy;</acronym> 2016 The Qt Company Ltd.
   Documentation contributions included herein are the copyrights of
   their respective owners.<br>    The documentation provided herein is licensed under the terms of the    <a href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation    License version 1.3</a> as published by the Free Software Foundation.<br>    Qt and respective logos are trademarks of The Qt Company Ltd.     in Finland and/or other countries worldwide. All other trademarks are property
   of their respective owners. </p>
</div>
</body>
</html>
