<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- qtlabscontrols-differences.qdoc -->
  <title>Differences between Qt Quick Controls | Qt Labs Controls 5.6</title>
  <link rel="stylesheet" type="text/css" href="style/offline-simple.css" />
  <script type="text/javascript">
    window.onload = function(){document.getElementsByTagName("link").item(0).setAttribute("href", "style/offline.css");};
  </script>
</head>
<body>
<div class="header" id="qtdocheader">
  <div class="main">
    <div class="main-rounded">
      <div class="navigationbar">
        <table><tr>
<td ><a href="../qtdoc/supported-platforms-and-configurations.html#qt-5-6">Qt 5.6</a></td><td ><a href="qtlabscontrols-index.html">Qt Labs Controls</a></td><td >Differences between Qt Quick Controls</td></tr></table><table class="buildversion"><tr>
<td id="buildversion" width="100%" align="right">Qt 5.6.0 Reference Documentation</td>
        </tr></table>
      </div>
    </div>
<div class="content">
<div class="line">
<div class="content mainContent">
<div class="sidebar">
<div class="toc">
<h3><a name="toc">Contents</a></h3>
<ul>
<li class="level2"><a href="#c-and-qml">C++ and QML</a></li>
<li class="level2"><a href="#styles">Styles</a></li>
<li class="level2"><a href="#modularity-and-simplicity">Modularity and Simplicity</a></li>
<li class="level2"><a href="#feature-comparison-table">Feature Comparison Table</a></li>
<li class="level2"><a href="#porting-qt-quick-controls-code">Porting Qt Quick Controls Code</a></li>
<li class="level1"><a href="#related-information">Related Information</a></li>
</ul>
</div>
<div class="sidebar-content" id="sidebar-content"></div></div>
<h1 class="title">Differences between Qt Quick Controls</h1>
<span class="subtitle"></span>
<!-- $$$qtlabscontrols-differences.html-description -->
<div class="descr"> <a name="details"></a>
<p>Qt Quick Controls were originally developed to support desktop platforms, with mobile and embedded support coming shortly afterwards. They have a very broad scope, in that they provide a styling system flexible enough to allow the development of applications that have either a platform-dependent or platform-independent style.</p>
<p>On embedded systems, where the hardware has limited resources, this approach can be inefficient. Qt Labs Controls were designed to solve this problem, using <a href="https://blog.qt.io/blog/2015/03/31/qt-quick-controls-for-embedded/">benchmarks</a> to guide the development.</p>
<a name="c-and-qml"></a>
<h3 >C++ and QML</h3>
<p>In many cases, the internal state of a control can be more efficiently processed in C++. For example, handling input events in C++ makes a difference for controls that would otherwise need to create internal MouseAreas and attached Keys objects.</p>
<a name="styles"></a>
<h3 >Styles</h3>
<p>Not only does handling events and logic in C++ increase performance, but it allows the visual QML layer to be a simple, declarative layer on top. This is reflected in the structure of the controls project: all visual implementations sit in the <i>imports</i> folder, so that users who want to create their own complete style can copy the folder and start tweaking. Read more about implementing a style plugin here.</p>
<p>In Qt Labs Controls, styles no longer provide components that are dynamically instantiated by controls, but controls themselves consist of item delegates that can be replaced. In effect, this means that delegates are Qt Quick items that are instantiated on the spot, as properties of the control, and are simply parented to the control.</p>
<a name="modularity-and-simplicity"></a>
<h3 >Modularity and Simplicity</h3>
<p>When it comes to more complex controls, it is sometimes better to split them up into separate building blocks. As an example, the complex <a href="../qtquickcontrols/qml-qtquick-controls-scrollview.html">ScrollView</a> control:</p>
<pre class="qml">

  <span class="type"><a href="../qtquickcontrols/qml-qtquick-controls-scrollview.html">ScrollView</a></span> {
      <span class="name">horizontalScrollBarPolicy</span>: <span class="name">Qt</span>.<span class="name">ScrollBarAlwaysOff</span>
      <span class="type"><a href="../qtquick/qml-qtquick-flickable.html">Flickable</a></span> {
          <span class="comment">// ...</span>
      }
  }

</pre>
<p>Is replaced with simple <a href="qml-qt-labs-controls-scrollbar.html">ScrollBar</a>/<a href="qml-qt-labs-controls-scrollindicator.html">ScrollIndicator</a> controls that can be attached to any Flickable:</p>
<pre class="qml">

  <span class="type"><a href="../qtquick/qml-qtquick-flickable.html">Flickable</a></span> {
      <span class="comment">// ...</span>
      <span class="name">ScrollBar</span>.vertical: <span class="name">ScrollBar</span> { }
  }

</pre>
<p>The API of Qt Labs Controls aims to be clean and simple. Common operations are easy, and more advanced ones are liberally documented with snippets that can be copied into your code.</p>
<a name="feature-comparison-table"></a>
<h3 >Feature Comparison Table</h3>
<div class="table"><table class="generic">
 <thead><tr class="qt-style"><th ></th><th >Qt Quick Controls</th><th >Qt Labs Controls</th></tr></thead>
<tr valign="top" class="odd"><td >Stylable delegates</td><td >Yes</td><td >Yes</td></tr>
<tr valign="top" class="even"><td >Pre-built native styles</td><td >Yes</td><td >No</td></tr>
<tr valign="top" class="odd"><td >Runtime style changes</td><td >Yes</td><td >Yes</td></tr>
<tr valign="top" class="even"><td >Can be used on Desktop</td><td >Yes</td><td >Yes <b>*</b></td></tr>
<tr valign="top" class="odd"><td >Can be used on Mobile</td><td >Yes</td><td >Yes</td></tr>
<tr valign="top" class="even"><td >Can be used on Embedded</td><td >Yes</td><td >Yes</td></tr>
<tr valign="top" class="odd"><td >Internal event handling</td><td >QML</td><td >C++</td></tr>
</table></div>
<p><b>* No hover support</b></p>
<a name="porting-qt-quick-controls-code"></a>
<h3 >Porting Qt Quick Controls Code</h3>
<p>The API of Qt Labs Controls is very similar to Qt Quick Controls, but it does come with some changes necessary to facilitate the improvements. The majority of changes are to do with styling; all of a control's delegates are now accessible in the control itself, instead of in a separate style object.</p>
<p>For example, to style a button in Qt Quick Controls:</p>
<pre class="cpp">

  Button {
      style: ButtonStyle {
          label: Label {
              // ..&#x2e;
          }
      }
  }

</pre>
<p>To style a button in Qt Labs Controls:</p>
<pre class="qml">

  <span class="type"><a href="qml-qt-labs-controls-button.html">Button</a></span> {
      <span class="name">label</span>: <span class="name">Label</span> {
          <span class="comment">// ...</span>
      }
  }

</pre>
<a name="preparing-for-migration"></a>
<h4 >Preparing for Migration</h4>
<p>With this in mind, a good way to prepare for a migration to Qt Quick Labs is to place each control that you have a custom style for in its own QML file. For example, the Qt Quick Controls button above could be moved to a file named Button.qml, and used in the following manner:</p>
<pre class="cpp">

  import &quot;controls&quot; as Controls

  Controls.Button {
      ..&#x2e;
  }

</pre>
<p>This works with both modules, and will reduce the amount of work needed when the migration begins.</p>
<a name="type-changes"></a>
<h4 >Type Changes</h4>
<div class="table"><table class="generic">
 <thead><tr class="qt-style"><th >Qt Quick Controls</th><th >Qt Labs Controls</th></tr></thead>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-action.html">Action</a></td><td >No equivalent; see <a href="../qtquick/qml-qtquick-shortcut.html">Shortcut</a> instead.</td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-applicationwindow.html">ApplicationWindow</a></td><td ><a href="qml-qt-labs-controls-applicationwindow.html">ApplicationWindow</a></td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-busyindicator.html">BusyIndicator</a></td><td ><a href="qml-qt-labs-controls-busyindicator.html">BusyIndicator</a></td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-button.html">Button</a></td><td ><a href="qml-qt-labs-controls-button.html">Button</a></td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-calendar.html">Calendar</a></td><td >No equivalent; see <a href="qml-qt-labs-calendar-monthgrid.html">MonthGrid</a>, <a href="qml-qt-labs-calendar-dayofweekrow.html">DayOfWeekRow</a> and <a href="qml-qt-labs-calendar-weeknumbercolumn.html">WeekNumberColumn</a> instead.</td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-checkbox.html">CheckBox</a></td><td ><a href="qml-qt-labs-controls-checkbox.html">CheckBox</a></td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-combobox.html">ComboBox</a></td><td ><a href="qml-qt-labs-controls-combobox.html">ComboBox</a></td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-exclusivegroup.html">ExclusiveGroup</a></td><td ><a href="qml-qt-labs-controls-buttongroup.html">ButtonGroup</a></td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-groupbox.html">GroupBox</a></td><td ><a href="qml-qt-labs-controls-groupbox.html">GroupBox</a>, or <a href="qtlabscontrols-customize.html#frame">Frame</a> if a title is not required.</td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-label.html">Label</a></td><td ><a href="qtlabscontrols-customize.html#label">Label</a></td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-menu.html">Menu</a></td><td ><a href="qml-qt-labs-controls-menu.html">Menu</a></td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-progressbar.html">ProgressBar</a></td><td ><a href="qml-qt-labs-controls-progressbar.html">ProgressBar</a></td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-radiobutton.html">RadioButton</a></td><td ><a href="qml-qt-labs-controls-radiobutton.html">RadioButton</a></td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-scrollview.html">ScrollView</a></td><td ><a href="qml-qt-labs-controls-scrollbar.html">ScrollBar</a>, <a href="qml-qt-labs-controls-scrollindicator.html">ScrollIndicator</a></td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-slider.html">Slider</a></td><td ><a href="qml-qt-labs-controls-slider.html">Slider</a></td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-spinbox.html">SpinBox</a></td><td ><a href="qml-qt-labs-controls-spinbox.html">SpinBox</a></td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-stack.html">Stack</a>, <a href="../qtquickcontrols/qml-qtquick-controls-stackview.html">StackView</a>, <a href="../qtquickcontrols/qml-qtquick-controls-stackviewdelegate.html">StackViewDelegate</a></td><td ><a href="qml-qt-labs-controls-stackview.html">StackView</a></td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-statusbar.html">StatusBar</a></td><td >No equivalent</td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-switch.html">Switch</a></td><td ><a href="qml-qt-labs-controls-switch.html">Switch</a></td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-tab.html">Tab</a>, <a href="../qtquickcontrols/qml-qtquick-controls-tabview.html">TabView</a></td><td ><a href="qml-qt-labs-controls-tabbar.html">TabBar</a> in combination with, for example, <a href="qml-qt-labs-controls-swipeview.html">SwipeView</a>.</td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-tableview.html">TableView</a></td><td >No equivalent</td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-textarea.html">TextArea</a></td><td ><a href="qml-qt-labs-controls-textarea.html">TextArea</a></td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-textfield.html">TextField</a></td><td ><a href="qml-qt-labs-controls-textfield.html">TextField</a></td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-toolbar.html">ToolBar</a></td><td ><a href="qml-qt-labs-controls-toolbar.html">ToolBar</a></td></tr>
<tr valign="top" class="odd"><td ><a href="../qtquickcontrols/qml-qtquick-controls-toolbutton.html">ToolButton</a></td><td ><a href="qml-qt-labs-controls-toolbutton.html">ToolButton</a></td></tr>
<tr valign="top" class="even"><td ><a href="../qtquickcontrols/qml-qtquick-controls-treeview.html">TreeView</a></td><td >No equivalent</td></tr>
</table></div>
<a name="related-information"></a>
<h2 id="related-information">Related Information</h2>
<ul>
<li><a href="../qtquick/qtquick-index.html">Qt Quick</a></li>
<li><a href="../qtquickcontrols/qtquickcontrols-index.html">Qt Quick Controls</a></li>
<li><a href="qt-labs-controls-qmlmodule.html">Qt Labs Controls QML Types</a></li>
</ul>
</div>
<!-- @@@qtlabscontrols-differences.html -->
        </div>
       </div>
   </div>
   </div>
</div>
<div class="footer">
   <p>
   <acronym title="Copyright">&copy;</acronym> 2016 The Qt Company Ltd.
   Documentation contributions included herein are the copyrights of
   their respective owners.<br>    The documentation provided herein is licensed under the terms of the    <a href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation    License version 1.3</a> as published by the Free Software Foundation.<br>    Qt and respective logos are trademarks of The Qt Company Ltd.     in Finland and/or other countries worldwide. All other trademarks are property
   of their respective owners. </p>
</div>
</body>
</html>
